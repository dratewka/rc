# Set up the prompt

export PATH=$PATH:~/bin
export GOPATH=/home/local/ALLEGROGROUP/pawel.zawistowski/lib/gocode


source /home/local/ALLEGROGROUP/pawel.zawistowski/.zsh/antigen/antigen.zsh

h=()
if [[ -r ~/.ssh/config ]]; then
    h=($h ${${${(@M)${(f)"$(cat ~/.ssh/config)"}:#Host *}#Host }:#*[*?]*})
fi
if [[ -r ~/.ssh/known_hosts ]]; then
    h=($h ${${${(f)"$(cat ~/.ssh/known_hosts{,2} || true)"}%%\ *}%%,*}) 2>/dev/null
fi
if [[ $#h -gt 0 ]]; then
    zstyle ':completion:*:ssh:*' hosts $h
    zstyle ':completion:*:slogin:*' hosts $h
fi


bindkey  "^[[1~"   beginning-of-line
bindkey  "^[[4~"   end-of-line

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme jreese

# Tell antigen that you're done.
antigen apply

